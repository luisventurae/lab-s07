package controllers;

import gui.Index;

/**
 *
 * @author Luis Ventura
 */
public class Init {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Index form = new Index();
        form.setVisible(true);
    }
    
}
