package controllers;

import bean.Sueldo;

/**
 *
 * @author Luis Ventura
 */
public class Calc extends Sueldo {

    int horasX;
    double sueldoB;

    public Calc(int horasX, double sueldoB) {
        this.horasX = horasX;
        this.sueldoB = sueldoB;

    }

    public Sueldo calcular() {
        Sueldo sueldo = new Sueldo();

        int horasBasicas = 40;
        int horasExtra = horasX;

        double montoExtraHr, sueldoMedio, impuestos, total;
        double montoBasicoHr = sueldoB / horasBasicas;

        if (horasExtra > 0) {
            montoExtraHr = montoBasicoHr * horasExtra * 2;
        }else {
            montoExtraHr = 0;
        }
        
        sueldo.setMontoHrsX(montoExtraHr);
        sueldoMedio = sueldoB + montoExtraHr;
        sueldo.setSueldo(sueldoMedio);

        if (sueldoMedio >= 2500) {
            impuestos = 0.25 * sueldoMedio;
        } else if (sueldoMedio >= 1500) {
            impuestos = 0.20 * sueldoMedio;
        } else if (sueldoMedio >= 1000) {
            impuestos = 0.15 * sueldoMedio;
        } else if (sueldoMedio >= 930) {
            impuestos = 0.10 * sueldoMedio;
        } else {
            impuestos = 0 * sueldoMedio;
        }
        sueldo.setImpuestos(impuestos);

        total = sueldoMedio - impuestos;
        sueldo.setTotal(total);

        return sueldo;
    }

}
