package bean;

/**
 *
 * @author Luis Ventura
 */
public class Sueldo {
    double montoHrsX, sueldo, impuestos, total;

    public double getMontoHrsX() {
        return montoHrsX;
    }

    public void setMontoHrsX(double montoHrsX) {
        this.montoHrsX = montoHrsX;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public double getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(double impuestos) {
        this.impuestos = impuestos;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
